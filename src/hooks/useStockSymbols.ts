import { useState, useEffect } from 'react';
import { useFinnhub } from 'react-finnhub'

interface StockSymbol {
  description: string;
  label: string;
  value: string;
}

const useStockSymbols = () => {
  const finnhub = useFinnhub();
  const [symbols, setSymbols] = useState<StockSymbol[]>([]);

  useEffect(() => {
    finnhub.stockSymbols("US").then((response) => {
      const formattedSymbols:StockSymbol[] = response.data.slice(0, 10).map((symbol) => ({ 
        label: symbol?.displaySymbol || '', 
        value: symbol?.displaySymbol || '', 
        description: '' 
      }));
      const newSymbols:StockSymbol[] = [{ label: 'BINANCE:BTCUSDT', value: 'BINANCE:BTCUSDT', description: '' }, ...formattedSymbols];
      setSymbols(newSymbols);
    });
  }, []);
  
  
  return {
    symbols: symbols ?? []
  }
}

export default useStockSymbols