import { useMemo } from 'react'

import type { ChartData } from 'chart.js';

import { Stock } from '../interfaces/stock';


const useGraphData = (stocks: Stock[]) => {
  
  const labels:string[] = useMemo(() => {
    return stocks.map((stock) => stock.s)
  }, [stocks]);

  const data = useMemo(() => {
    const data:ChartData<'line'> = {
      labels,
      datasets: [{
        label: 'Stocks',
        data: stocks.map((stock) => stock.p),
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      }],
    };

    return data;
  }, [stocks, labels])

  return {
    data, 
  }
}

export default useGraphData