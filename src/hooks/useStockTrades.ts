import { useEffect } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

const useStockTrades = (stockToWatch: string) => {
  const websocketUrl:string = `${import.meta.env.VITE_FINNHUB_WEBSOCKET_URL}?token=${import.meta.env.VITE_FINNHUB_TOKEN}`;
  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(websocketUrl);
  
  useEffect(() => {
    if (readyState === ReadyState.OPEN) {
      sendJsonMessage({'type':'subscribe', 'symbol': stockToWatch});
    }

    return () => {
      sendJsonMessage({'type':'unsubscribe', 'symbol': stockToWatch});
    }
  }, [readyState, stockToWatch]);


  return {
    stocks: lastJsonMessage?.data || []
  }
}

export default useStockTrades;