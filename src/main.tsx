import React from 'react'
import ReactDOM from 'react-dom/client'

import {NextUIProvider} from "@nextui-org/react";
import { finnhubClient, FinnhubProvider } from 'react-finnhub'

import App from './App.tsx'
import './index.css'

const client = finnhubClient(import.meta.env.VITE_FINNHUB_TOKEN);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <NextUIProvider>
      <FinnhubProvider client={client}>
        <main className="dark text-foreground bg-background h-screen">
          <App />
        </main>
      </FinnhubProvider>
    </NextUIProvider>
  </React.StrictMode>,
)
