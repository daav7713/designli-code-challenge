import { Divider, Skeleton } from "@nextui-org/react"

const StockItemSkeleton = () => {
  return (
    <div className="flex items-center w-72 flex-shrink-0">
      <div className="w-full grid grid-cols-2 gap-4 py-2 px-4 ">
      <Skeleton className="rounded-lg">
        <div className="h-3 w-3/5 rounded-lg bg-gray-200"></div>
      </Skeleton>
      <Skeleton className="rounded-lg">
        <div className="h-3 w-3/5 rounded-lg bg-gray-200"></div>
      </Skeleton>
      <Skeleton className="rounded-lg">
        <div className="h-3 w-3/5 rounded-lg bg-gray-200"></div>
      </Skeleton>
      </div>
      <Divider orientation="vertical" className="h-12" />
    </div>
  )
}

export default StockItemSkeleton