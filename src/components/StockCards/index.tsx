import { Stock } from "../../interfaces/stock"
import StockItemCard from "../StockItemCard"
import StockItemSkeleton from "../StockItemSkeleton"

interface Props {
  stocks: Stock[],
  priceAlert: string,
}

const StocksCards = ({ stocks, priceAlert }:Props) => {
  return (
    <div className="stocks w-full flex items-center justify-start overflow-x-auto">
      {
        stocks.length > 0 ? (
          <>
            {
              stocks.map((stock, index) => (
                <StockItemCard key={`${stock.s}_${index}`} stock={stock} priceAlert={priceAlert} />
              ))
            }
          </>
        ) : (
          <div className="flex my-2">
            {
              [1, 2, 3, 4, 5].map((value) => (
                <StockItemSkeleton key={value} />
              ))
            }
          </div>
        )
      }

    </div>
  )
}

export default StocksCards