import { Divider } from '@nextui-org/react'
import { Stock } from '../../interfaces/stock'
import classNames from 'classnames'

interface Props {
  stock: Stock,
  priceAlert: string
}

const StockItemCard = ({ stock, priceAlert }:Props) => {
  return (
    <div className="flex items-center w-72 flex-shrink-0">
      <div className="grid grid-cols-2 gap-4 py-2 px-4">
        <div>{stock.s}</div>
        <div className={classNames('text-right', {
          'text-green-500': stock.p > parseFloat(priceAlert),
          'text-red-500': stock.p < parseFloat(priceAlert)
        })}>{stock.p}</div>
        <div>{stock.v}%</div>
      </div>
      <Divider orientation="vertical" className="h-12" />
    </div>
  )
}

export default StockItemCard