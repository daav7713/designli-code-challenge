import {
  Chart as ChartJS,
  ChartOptions,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import useGraphData from '../../hooks/useGraphData';
import { Stock } from '../../interfaces/stock';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options:ChartOptions<'line'> = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Trades - Last Price Updates',
    },
  },
};


interface Props {
  stocks: Stock[];
}

const StocksGraph = ({ stocks }: Props) => {
  const { data } = useGraphData(stocks);

  return (
    <Line options={options} data={data} />
  )
}

export default StocksGraph